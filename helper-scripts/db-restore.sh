#!/bin/bash
set -euo pipefail
cd `dirname "$0"`

dumpFile=${1:?first param must be path to dump/backup file}

cat $dumpFile | docker exec -i bdrs_db sh -c 'pg_restore -v -U $POSTGRES_USER -d $POSTGRES_DB --clean'
