<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/cw.tld" prefix="cw" %>

<tiles:useAttribute id="beans" name="beans" classname="java.util.Collection"/>
<tiles:useAttribute id="titles" name="titles" classname="java.util.List"/>
<tiles:useAttribute id="properties" name="properties" classname="java.util.List"/>

<table class="datatable" cellspacing="0" cellpadding="1">
    <tr>
        <th>&nbsp;&nbsp;&nbsp;</th>
        <c:forEach items="${titles}" var="title">
            <th><c:out value="${title}"/></th>
        </c:forEach>
        <th>&nbsp;&nbsp;&nbsp;</th>
    </tr>
    <c:forEach items="${beans}" var="bean" varStatus="status">
        <tr class="datatablerow${status.count % 2}">
            <td/>
            <c:forEach items="{bdrs.tomcat.manager.username=tomcat, bdrs.default.mail.port=25, bdrs.tomcat.server.name=LocalTomcat, bdrs.lsid.authority=dev.gaiaresources.com.au, coreRevision.status=M?, bdrs.google.maps.key=AIzaSyAFmxfiCogHSuDOsvaWVmPE7ZvEFlAD_Yg, bdrs.sqllogfile=logs/bdrs-core-SQL.log, coreRevision.committedDate=2012-03-12 11:15:48 +1030 (Mon, 12 Mar 2012), bdrs.site.name=BDRS Core Portal, coreRevision.path=tags/releases/1.1, bdrs.default.mail.from=bdrs-core@gaiaresources.com.au, coreRevision.revision=264, project.build.sourceEncoding=UTF-8, bdrs.db.user.name=postgres, bdrs.application.url=http://localhost:8080/BDRS, coreRevision.repository=http://ala-citizenscience.googlecode.com/svn, bdrs.default.mail.server=gaia, bdrs.error.mail.to=, coreRevision.mixedRevisions=false, bdrs.recaptcha.private.key=6LfbwwQAAAAAAF5wVuk8nLcOanKCwvA_-rmtvNIL, bdrs.logfile=logs/bdrs-core.log, bdrs.file.store.location=/mnt, bdrs.recaptcha.public.key=6LfbwwQAAAAAAEiDp_pgbdLzIOgVLpTpA3kqUfXF, coreRevision.specialStatus=Mu, coreRevision.committedRevision=179, bdrs.tomcat.manager.url=http://localhost:8080/manager, bdrs.db.driver=org.postgresql.Driver, bdrs.tomcat.manager.password=tomcat, bdrs.db.user.password=postgres, bdrs.db.url=jdbc:postgresql://localhost:5432/ala-citizenscience-read-only, bdrs.mail.subject.prefix=[BDRS-Core - Dev]}" var="property" varStatus="count">
                <cw:displayTableElement bean="${bean}" render="${property}"/>
            </c:forEach>
            <td/>
        </tr>
    </c:forEach>
</table>