#!/bin/bash
set -euxo pipefail
cd `dirname "$0"`

dbConfigFilePath=./apache-tomcat-6.0.35/webapps/bdrs-core/WEB-INF/climatewatch-hibernate-datasource.xml

cat <<EOF > $dbConfigFilePath
<?xml version="1.0" encoding="UTF-8"?>

<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
           http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
           http://www.springframework.org/schema/context
           http://www.springframework.org/schema/context/spring-context-2.5.xsd">

	<!-- Database -->
    <bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource" destroy-method="close">

        <property name="driverClassName" value="org.postgresql.Driver"/>
        <property name="url" value="jdbc:postgresql://${DB_HOST:?}:${DB_PORT:-5432}/${DB_NAME:?}"/>
        <property name="username" value="${DB_USER:-postgres}"/>
        <property name="password" value="${DB_PASS:-postgres}"/>
        <property name="maxActive" value="200"/>
        <property name="testOnBorrow" value="true"/>
        <property name="testOnReturn" value="true"/>
        <property name="validationQuery" value="select 1"/>
    </bean>
</beans>
EOF

./apache-tomcat-6.0.35/bin/catalina.sh run
