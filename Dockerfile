FROM openjdk:7

RUN useradd --create-home --shell /bin/bash trenduser
RUN mkdir -p /mnt && chown trenduser:trenduser /mnt
USER trenduser
WORKDIR /home/trenduser
ADD --chown=trenduser:trenduser ./app/ ./

EXPOSE 8080
ENTRYPOINT ["/bin/bash", "entrypoint.sh"]
