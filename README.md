> The TREND citizen science BDRS app's server

# Quickstart
  1. copy runner file `cp run.sh.example run.sh`
  1. `chmod +x run.sh`
  1. provide secrets: `vim run.sh`
  1. start the stack: `./run.sh`
  1. the app container will probably complain. If it's a failed connection,
     restart the stack/container. If it's about missing DB content. Once you
     restore a snapshot to the DB, restart the stack/container and it should
     start fine.
  1. if you need to restore DB:
      ```bash
      ./helper-scripts/db-restore.db the-dump.backup
      ```
  1. if you need to restore the files:
      ```bash
      sudo su
      cd /var/lib/docker/volumes/trend-app-docker_appdata/_data/
      cp path/to/backups/au/ .
      ```

# History
This was lifted from an old VM and Docker-ised with minimal changes. It may be
possible to remove all the Tomcat binary from this repo and either use a Tomcat
base image or pull the binary during image build.

Most of the magic is in `app/apache-tomcat-6.0.35/webapps/bdrs-core`.

# Known issues
  1. the recaptcha on the sign up page 404s, so if you try to continue the
     server gives a 500. This is fine because users sign up via the Android app
     so this logic isn't used.
  1. logging might not be working. I tried to get everything going to stdout but
     I'm not sure if it is.
